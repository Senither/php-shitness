<?php 

function paragraph($text, $styling = [])
{
    $style = '';
    foreach ($styling as $tag => $value) {
        $style .= "{$tag}: {$value};";
    }
    
    return "<p style='{$style}'>{$text}</p>";
}

// function paragraphWithoutArgs()
// {
//     $style = '';
//     foreach ($styling as $tag => $value) {
//         $style .= "{$tag}: {$value};";
//     }
    
//     return "<p style='{$style}'>{$text}</p>";
// }