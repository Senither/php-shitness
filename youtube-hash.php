<?php 

class IdGenerator
{
    protected $characters;

    public function __construct()
    {
        $this->characters .= '0123456789';
        $this->characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $this->characters .= 'abcdefghijklmnopqrstuvwxyz';
        $this->characters .= '-_';
    }

    public function build($length = 16)
    {
        return substr(str_shuffle(str_repeat($this->characters(), $length)), 0, $length);
    }

    protected function characters()
    {
        $base  = $this->characters;
        $base .= base64_encode(microtime());
        $base .= microtime();
        $base .= base64_encode($base);

        return rtrim($base, '=');
    }
}

$generator = new IdGenerator();

$dbh = new PDO('mysql:host=localhost;dbname=test', 'username', 'password');
$stmt = $dbh->prepare('INSERT INTO `cs_table` SET `token` = :token');

set_time_limit(99999999);

while (true) {
    $stmt->bindParam(':token', $generator->build(2));
    $stmt->execute();
}
