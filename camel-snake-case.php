<?php 

function str_studly($value)
{
    return str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $value)));
}

function str_camel($value)
{
    return lcfirst(str_studly($value));
}

function str_snake($value, $delimiter = '_')
{
    if (! ctype_lower($value)) {
        $value = preg_replace('/\s+/u', '', $value);
        $value = mb_strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1'.$delimiter, $value));
    }

    return $value;
}

$string = str_studly('Lorem ipsum dolor sit amet, consectetur adipisicing elit.');

echo $string . '<br>' . str_camel($string) . '<br>' . str_snake($string);
