<?php 

class Person
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function sayHelloTo(Person $person)
    {
        return $this->name.' says hello to '.$person->getName();
    }

    public function countTo($number = 10)
    {
        if ($number < 1) {
            $number = 10;
        }

        $string = $this->name.' counts... ';

        foreach (range(1, $number) as $num) {
            $string .= $num.', ';
        }
        return trim($string, ', ');
    }
}

$alexis = new Person('Alexis');
$umbra  = new Person('Umbra');

echo $alexis->sayHelloTo($umbra);
echo '<br>';
echo $umbra->countTo(8);
echo '<br>';
echo $alexis->countTo(7);
