<?php 

function str_random($length = 16)
{
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
}

$data['2']       = str_random(2);
$data['4']       = str_random(4);
$data['8']       = str_random(8);
$data['16']      = str_random(16);
$data['32']      = str_random(32);
$data['64']      = str_random(64);
$data['default'] = str_random();
